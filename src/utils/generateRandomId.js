const generateRandomId = () => {
    return Math.ceil(Math.random()*99999999);
};

export default generateRandomId;
