import { useState } from 'react';
import { ThemeProvider, Container, CssBaseline, Switch, FormGroup, FormControlLabel, Tooltip , Box} from "@mui/material";
import DarkModeIcon from '@mui/icons-material/DarkMode';
import Theme, { DarkTheme } from "./components/Theme";
import Title from "./components/Title";
import ManageItems from "./components/ManageItems";

const App = () => {
    const [checked, setChecked] = useState(false);

    const handleChange = () => {
        setChecked(!checked);
    };
    
    return (
        <ThemeProvider theme={checked? DarkTheme: Theme}>
            <CssBaseline />
            <Container sx={{ display:"flex", flexDirection:"column", mt:10 }}>
                <Box sx={{ display:"flex", justifyContent:"flex-end" }}>
                    <FormGroup >
                        <Tooltip title="Dark Mode" >
                            <FormControlLabel control={<Switch checked={checked} onChange={handleChange} />} label={<DarkModeIcon />} />
                        </Tooltip>
                    </FormGroup>
                </Box>
                <Box sx={{ display: "flex", justifyContent:"center", alignItems:"center", flexDirection: "column" }} >
                    <Title />
                    <ManageItems />
                </Box>
            </Container>
        </ ThemeProvider>
    );
};

export default App;
