import { createTheme } from "@mui/material";

const Theme = createTheme({
    palette: {
        primary: {
            main: "#4CBB17"
        },
        secondary: {
            main: "#dbdbdb"
        },
        componentBackground: {
            main: "#ffffff"
        },
        button: {
            primary: "#ffffff"
        }
    }
});

const DarkTheme = createTheme({
    palette: {
        mode: "dark",
        primary: {
            main: "#4CBB17"
        },
        secondary: {
            main: "#dbdbdb"
        },
        componentBackground: {
            main: "#000000"
        },
        button: {
            primary: "#ffffff"
        }
    }
});

export default Theme;
export { DarkTheme };
