import { useState, useEffect } from 'react';
import { Box, Checkbox, Typography, Link } from "@mui/material";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import EditIcon from '@mui/icons-material/Edit';
import ItemModal from './ItemModal';
import getStatusBoolean from '../utils/getStatusBoolean';

const ItemShow = ({ value, onChange, onDelete }) => {
    const isCompleted = getStatusBoolean(value.status);
    const [checked, setChecked] = useState(isCompleted);
    const [onModal, setOnModal] = useState(false);

    useEffect(() => {
        setChecked(getStatusBoolean(value.status));
    }, [value]);

    let titleStyle = {};

    if ( checked ) {
        titleStyle = {
            textDecorationLine: "line-through"
        };
    };
    
    const toggleModal = () => {
        setOnModal(!onModal);
    };

    const closeModal = () => {
        setOnModal(false);
    };

    const handleCheckboxCheck = () => {
        const newCheck = !checked;
        setChecked(newCheck);
        const statusValue = newCheck? 1: 0;
        const newItem = {...value, status: statusValue};
        onChange(newItem);  
    };

    const handleDeleteClick = () => {
        onDelete(value);
    };

    const handleEditClick = () => {
        toggleModal();
    };

    return (
        <Box sx={{ display:"flex", p:2, border:"1px solid", flexDirection:"column", backgroundColor:"componentBackground.main" }}>
            <Box sx={{ display:"flex", flexDirection: "row", justifyContent: "space-between" }} >
                <Box sx={{ display:"flex", flexDirection: "row" }}>
                    <Checkbox checked={checked} onChange={handleCheckboxCheck}/>
                    <Typography variant="h5" sx={titleStyle}>
                        {value.title}
                    </Typography>
                </Box>
                <Box sx={{ display:"flex", flexDirection: "row", gap: 1 }}>
                    <Link component="button" onClick={handleEditClick} >
                        <EditIcon/>
                    </Link>
                    <Link component="button" onClick={handleDeleteClick} >
                        <DeleteForeverIcon/>
                    </Link>
                </Box>
            </Box>
            <Typography variant="body1">{value.description}</Typography>
            <ItemModal value={onModal} onChange={closeModal} onSubmit={onChange} item={value} buttonText="Modify Item" ></ItemModal>
        </Box>
    );
};

export default ItemShow;
