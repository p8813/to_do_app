import { useState, useEffect } from 'react';
import { Box, TextField } from "@mui/material";

const SearchBar = ({ onChange }) => {
    const [ searchTerm, setSearchTerm ] = useState("");

    useEffect(()=>{
        onChange(searchTerm);
    }, [searchTerm, onChange])

    const handleChange = (event) => {
        setSearchTerm(event.target.value);
    };

    return (
        <Box>
            <TextField label="Search field" type="search" value={searchTerm} onChange={handleChange} sx={{ mt:3, width:600 }} />
        </Box>
    );
};

export default SearchBar;
