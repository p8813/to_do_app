import { Box, Typography } from "@mui/material";
import ItemShow from "./ItemShow";

const ItemList = ({ value, selection, onChange, onDelete }) => {
    const renderedItems = value.map((item) => {
        const itemShow = <ItemShow key={item.id} value={item} onChange={onChange} onDelete={onDelete} />;
        if (selection === -1 ) {
            return itemShow;
        } else {
            return item.status === selection ? itemShow : null;
        };
    });

    const content = renderedItems.length > 0 ? renderedItems : <Typography sx={{ p:2, display:"flex", justifyContent:"center", fontWeight:700 }}>No TODO items. Yay!</Typography>;

    return (
        <Box sx={{ display:"flex", flexDirection:"column", gap: 1.5, p: 3, my: 2, backgroundColor: 'secondary.main', width: 600, border: "0.13rem solid", borderRadius: 2 }}>
            {content}
        </Box>
    );
};

export default ItemList;
