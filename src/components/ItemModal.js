import { useState, useEffect } from 'react';
import { Box, Button, Modal, TextField, Typography, Select, MenuItem, FormControl, InputLabel } from "@mui/material";
import generateRandomId from "../utils/generateRandomId"

const ItemModal = ({ value, onChange, onSubmit, item, buttonText }) => {
    const defaultTitle = "";
    const defaultDescription = "";
    const defaultStatus = 0;

    const [title, setTitle] = useState(defaultTitle);
    const [description, setDescription] = useState(defaultDescription);
    const [status, setStatus] = useState(defaultStatus); 

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: "translate(-50%, -50%)",
        backgroundColor: "componentBackground.main",
        width: 400,
        border: "1px solid",
        borderRadius: 4,
        p: 4,
        display: "flex",
        flexDirection: "column",
        gap: 3
    };

    useEffect(() => {
        if (typeof item === "object") {
            setTitle(item.title);
            setDescription(item.description);
            setStatus(item.status);
        };
    }, [item]);

    const handleChangeTitle = (event) => {
        setTitle(event.target.value);
    };

    const handleChangeDescription = (event) => {
        setDescription(event.target.value);
    };

    const handleStatusSelection = (event) => {
        setStatus(event.target.value);
    };

    const resetInputs = () => {
        setTitle(defaultTitle);
        setDescription(defaultDescription);
        setStatus(defaultStatus);
    };

    const handleSubmitButton = () => {
        const id = typeof item === "object" ? item.id : generateRandomId();
        onSubmit(
            {
                id: id,
                title: title,
                description: description,
                status: status
            }
        );
        onChange();
        if (typeof item !== "object"){
            resetInputs();
        };
    };

    return (
        <Box>
            <Modal open={value} onClose={onChange}>
                <Box sx={style}>
                    <Typography variant="h6">
                        Todo Item
                    </Typography>
                    <TextField defaultValue={title} label="Title*" onChange={handleChangeTitle} variant="standard"/>
                    <TextField defaultValue={description} label="Description" onChange={handleChangeDescription} multiline maxRows={4}/>
                    <Box sx={{ display:"flex", justifyContent:"space-between" }} >
                        <Button
                            disabled={title === "" ? true : false}
                            variant="outlined"
                            onClick={handleSubmitButton}
                            sx={{ Color: "secondary.main", textTransform:"none" }}
                        >
                            {buttonText}
                        </Button>
                        <FormControl sx={{ width:150 }}>
                            <InputLabel >Status</InputLabel>  
                            <Select value={status} label="Status" onChange={handleStatusSelection}>
                                <MenuItem value={0}>Incomplete</MenuItem>
                                <MenuItem value={1}>Completed</MenuItem>
                            </Select>
                        </FormControl>
                    </Box>
                </Box>
            </Modal>
        </Box>
    );
};

export default ItemModal;
