import { useState, useCallback } from 'react';
import { Box, Button, MenuItem, Select } from "@mui/material";
import ItemList from "./ItemList";
import ItemModal from './ItemModal';
import SearchBar from "./SearchBar";

const ManageItems = () => {
    const [onModal, setOnModal] = useState(false);
    const [items, setItems] = useState([]);
    const [searchItems, setSearchItems] = useState(null);
    const [statusSelect, setStatusSelect] = useState(-1);

    const toggleModal = () => {
        setOnModal(!onModal);
    };

    const closeModal = () => {
        setOnModal(false);
    };

    const addItem = (newItem) => {
        setItems([...items, newItem]);
    };

    const editItem = (modifiedItem) => {
        const updatedItems = items.map((item) => {
            if (item.id === modifiedItem.id) {
                return modifiedItem;
            } else{
                return item;
            };
        });
        setItems(updatedItems);
    };

    const removeItem = (itemToRemove) => {
        const updatedItems = items.filter((item) => {
            return item.id !== itemToRemove.id;
        });
        setItems(updatedItems);
    };

    const handleStatusSelection = (event) => {
        setStatusSelect(event.target.value);
    };

    const handleSearchItems = useCallback((searchTerm) => {
        const modifiedItems = items.filter((item) => {
            return item.title.toLowerCase().startsWith(searchTerm.toLowerCase());
        });
        setSearchItems(modifiedItems);
    }, [items]);

    const itemsToDisplay = Array.isArray(searchItems) ? searchItems : items;

    return (
        <Box sx={{ mt: 8 }}>
            <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                <Button variant="contained" onClick={toggleModal} sx={{ color:"button.primary", textTransform:"none", fontWeight:600 }} >Add Item</Button>
                <Select value={statusSelect} onChange={handleStatusSelection} sx={{ width: 150 }} >
                    <MenuItem value={-1}>All</MenuItem>
                    <MenuItem value={0}>Incomplete</MenuItem>
                    <MenuItem value={1}>Completed</MenuItem>
                </Select>
            </Box>
            <SearchBar onChange={handleSearchItems} />
            <ItemModal value={onModal} onChange={closeModal} onSubmit={addItem} item="" buttonText="Add Item" ></ItemModal>
            <ItemList value={itemsToDisplay} selection={statusSelect} onChange={editItem} onDelete={removeItem} />
        </Box>
    );
};

export default ManageItems;
