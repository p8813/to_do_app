import { Box, Typography } from "@mui/material"

const Title = () => {
    return (
        <Box>
            <Typography variant="h1" sx={{ fontFamily: "cursive", fontWeight: 700, color: "primary.main" }} >
                To Do App
            </Typography>
        </Box>
    );
};

export default Title;
